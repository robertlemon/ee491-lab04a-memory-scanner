#Build a Memory Scanner program

CC     = gcc
CFLAGS = -g -Wall

TARGET = MemoryScanner

all: $(TARGET)

MemoryScanner: lab4.c charCounting.c
	$(CC) $(CFLAGS) -o $(TARGET) lab4.c charCounting.c

clean:
	rm $(TARGET)

test: all
	./$(TARGET)
