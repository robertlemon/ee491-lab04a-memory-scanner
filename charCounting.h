///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file charCounting.h
/// @version 1.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date 10_feb_2021
///////////////////////////////////////////////////////////////////////////////

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Global variables 
extern int numBytes;
extern int numChar;
const extern char searchChar;

// Function prototypes
void charCount(char *start, char *end);
