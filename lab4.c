///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file lab4.c
/// @version 1.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date 10_feb_2021
///////////////////////////////////////////////////////////////////////////////

// Header file
#include "lab4.h"

// Global variables 
int numBytes;
int numChar;
const char searchChar = 'A';

/**
 * The main function for the MemoryScanner program.  This function opens
 * [/proc/self/maps], reads each line, and searches the associated 
 * section of memory, counting the amount of times a specific character
 * is present.
 */
int main(int argc, char *argv[]) {
   
   int i;
   int memSize;
   int lineCount = 0;

   char ch;
   char start[12+1]; // chars + end of string
   char end[12+1];   // chars + end of string
   char perms[4+1];  // chars + end of string
   char vvar[4+1];   // chars + end of string
   char *programName = argv[0] + 2; // removes './'

   bool keepScanning = true;

   FILE * file;

   // Attempt to open the file, otherwise exit
   if ( (file = fopen("/proc/self/maps", "r")) == NULL ) {
      fprintf(stderr, "%s: Can't open [/proc/self/maps]\n", programName);
      exit(EXIT_FAILURE);
   }

   // Read file
   while(keepScanning) {
   
      memSize = 0;

      // Copy starting address to start
      for (i = 0; i < 12; i++) {
         // Put ch back if address isn't 48bits.
         if ( (ch = fgetc(file)) == '-') {
            ungetc(ch, file);
            break;
         }
         start[i] = ch;
         memSize++;
      }
      start[i] = '\0'; // End string

      // Check for 64bit address (end of file)
      if ( (ch = fgetc(file)) != '-' ) {
         keepScanning = false;
         break;
      }

      // Copy ending address to end
      for (i = 0; i < memSize; i++) {
         end[i] = fgetc(file);
      }
      end[i] = '\0'; // End string

      // Remove space between address and perms
      ch = fgetc(file);

      // Copy perm settings to perms
      for (i = 0; i < 4; i++) {
         perms[i] = fgetc(file);
      }
      perms[4] = '\0';

      // Clear rest of line
      while ( (ch = fgetc(file)) != '\n' ) {
         // Check if trying to access [vvar] memory
         if ( ch == '[' ) {
            vvar[0] = fgetc(file);
            vvar[1] = fgetc(file);
            vvar[2] = fgetc(file);
            vvar[3] = fgetc(file);
            vvar[4] = '\0';
            if (strcmp(vvar, "vvar") == 0) {
               perms[0] = '-';
            }
         }
      }

      // Count and print memory if readable
      if (perms[0] == 'r') {
         charCount(start, end);
         printf("%2d:  %12s - %12s %s  Number of bytes read [%d]\tNumber of '%c' is [%d]\n",
            lineCount++, start, end, perms, numBytes, searchChar, numChar);
      }
   }

   // Attempt to close file, otherwise exit
   if (fclose(file) == EOF) {
      fprintf(stderr, "%s: Can't close [/proc/self/maps]\n", programName);
      exit(EXIT_FAILURE);
   }

   return EXIT_SUCCESS;
}
