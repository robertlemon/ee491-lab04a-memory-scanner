///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - MemoryScanner
///
/// @file charCounting.c
/// @version 1.0
///
/// @author Robert Lemon <rlemon@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date 10_feb_2021
///////////////////////////////////////////////////////////////////////////////

// Header file
#include "charCounting.h"

/**
 * A function that iterates over a specified section of memory
 * and counts the number of occurances of a specified character.
 * It keeps track of the counts via global variables.
 *
 * INPUT: two character arrays (start and end pointers)
 * OUTPUT: none
 */
void charCount(char *start, char *end) {

   long temp;
   void * ptr;
   void * ptrEnd;

   // Reset counting variables
   numChar = 0;
   numBytes = 0;

   // Convert hex strings to pointer
   temp = strtol(start, NULL, 16);
   ptr = (void *)temp;

   temp = strtol(end, NULL, 16);
   ptrEnd = (void *)temp;

   // Iterate from start to stop
   while (ptr < ptrEnd) {
      if ( *(char *)ptr == searchChar ) {
         numChar++;
      }
      numBytes++;
      ptr++;
   }

   return;
}
